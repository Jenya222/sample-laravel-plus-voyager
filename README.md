# TODO #
* php artisan migrate
* php artisan db:seed
* php artisan storage:link
* php artisan vendor:publish voyager_avatar

* php artisan iseed  data_types,data_rows,menus,menu_items,users,user_roles,roles,permissions,permission_role,settings,translations,voip_servers --force


# Start laravel + voyager #
* clone the project
* find all entries 'webrtc' and replace by the new name
* docker-compose up -d
* docker exec -it 'new name' bash

* composer install
* cp .env_dev .env
* php artisan voyager:install
* php artisan db:seed
