<?php


namespace App\Imports;


use App\Phone;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

/**
 * Class PhonesImport
 * @package App\Imports
 */
class PhonesImport implements ToCollection
{
    /**
     * Phone base id which was assigned to these numbers
     *
     * @var int $phoneBaseId
     */
    private $phoneBaseId;

    /**
     * PhonesImport constructor.
     * @param $phoneBaseId
     */
    public function __construct(int $phoneBaseId)
    {
        $this->phoneBaseId = $phoneBaseId;
    }

    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows) : void
    {
        foreach ($rows as $row)
        {
            Phone::create([
                'number' => (string)trim($row[0],';'),
                'phone_base_id' => $this->phoneBaseId,
            ]);
        }
    }
}