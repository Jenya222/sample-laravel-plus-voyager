<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$VjLwlVZjgow/iwehkJN0wuGoR2wntV75D/IpxZ0sC7RgDvUtXjPSK',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_at' => '2019-09-13 23:30:55',
                'updated_at' => '2019-09-13 23:30:56',
            ),
        ));
        
        
    }
}